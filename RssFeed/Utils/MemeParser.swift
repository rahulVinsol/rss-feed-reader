//
//  XmlParser.swift
//  RssFeed
//
//  Created by Rahul Rawat on 15/06/21.
//

import Foundation

class MemeParser: NSObject, XMLParserDelegate {
    private var memes = [Meme]()
    
    private var title = ""
    private var imageUrl = ""
    
    private var completionHandler: (([Meme])->Void)?
    
    private var currentElement: String = ""
    
    func parse(inputStream: InputStream, completionHandler: @escaping ([Meme])-> Void) {
        self.completionHandler = completionHandler
        
        let parser = XMLParser(stream: inputStream)
        parser.delegate = self
        parser.parse()
    }
    
    func parse(url: URL, completionHandler: @escaping ([Meme])-> Void) {
        self.completionHandler = completionHandler
        
        let parser = XMLParser(contentsOf: url)
        parser?.delegate = self
        parser?.parse()
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        currentElement = elementName
        switch currentElement {
        case "entry":
            title = ""
            imageUrl = ""
        case "media:thumbnail":
            imageUrl = attributeDict["url"] ?? ""
        default:
            break
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        switch currentElement {
        case "title":
            self.title += string
        default:
            break
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        switch elementName {
        case "entry":
            if !self.title.isEmpty && !self.imageUrl.isEmpty {
                memes.append(Meme(title: self.title, imageUrl: self.imageUrl))
            }
        default:
            break
        }
    }
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        print("\(parseError.localizedDescription)")
    }
    
    func parserDidEndDocument(_ parser: XMLParser) {
        completionHandler?(self.memes)
    }
}
