//
//  ViewController.swift
//  RssFeed
//
//  Created by Rahul Rawat on 15/06/21.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var toggleDataSource: UISwitch!
    
    private var shouldUseOnlineContent = false
    
    private var memes = [Meme]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        
        reloadMemes()
    }
    
    @IBAction func contentSwitched(_ sender: UISwitch) {
        self.shouldUseOnlineContent = sender.isOn
        reloadMemes()
    }
    
    func reloadMemes() {
        let parser = MemeParser()
        
        let completionHandler: ([Meme]) -> Void = { [weak self] memes in
            guard let self = self else { return }
            self.memes = memes
            self.tableView.reloadData()
        }
        
        let url = shouldUseOnlineContent ? URL(string: RSS_URL)! : URL(fileURLWithPath: Bundle.main.path(forResource: LOCAL_ASSET_FILE_NAME, ofType: LOCAL_ASSET_EXTENSION)!)
        
        parser.parse(url: url, completionHandler: completionHandler)
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        memes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.IDENTIFIER) as! TableViewCell
        
        let meme = memes[indexPath.row]
        cell.setup(with: meme)
        
        return cell
    }
}
