//
//  Meme.swift
//  RssFeed
//
//  Created by Rahul Rawat on 15/06/21.
//

import Foundation

struct Meme {
    let title: String
    let imageUrl: String
}
