//
//  TableViewCell.swift
//  RssFeed
//
//  Created by Rahul Rawat on 15/06/21.
//

import UIKit

class TableViewCell: UITableViewCell {
    static let IDENTIFIER = "cell"

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var memeImage: UIImageView!
    
    func setup(with meme: Meme) {
        titleLabel.text = meme.title
        ImageDownloadUtility.shared.download(url: meme.imageUrl, completionHandler: { [weak self] image in
            self?.memeImage.image = image
        })
    }
}
