//
//  Constants.swift
//  RssFeed
//
//  Created by Rahul Rawat on 24/06/21.
//

import Foundation

let RSS_URL = "https://www.reddit.com/r/ProgrammerHumor.rss?limit=100"
let LOCAL_ASSET_FILE_NAME = "xml"
let LOCAL_ASSET_EXTENSION = "xml"
